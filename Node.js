var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var players = [];

server.listen(8080, function(){
	console.log("Server is now running...");
});

io.on('connection', function(socket){
	console.log("Player Connected : " + socket.id);
	socket.emit('socketID', {id : socket.id});
	socket.emit('getPlayers', players);
	socket.broadcast.emit('newPlayer', {id : socket.id});
	players.push(new player(socket.id, 20));
	socket.on('disconnect', function(){
		console.log("Player Disconnected : " + socket.id);
		socket.broadcast.emit("enemyDisconnected", {id : socket.id});
		for(var i = 0; i < players.length; i++) {
		    if (players[i].id == socket.id) {
		        players.splice(i, 1);
		    }
		}
	});
    socket.on('playerMoved', function(data) {
        //data.id = socket.id;
        socket.broadcast.emit('enemyMoved', data)
    });
    socket.on('createShot', function(data) {
        //data.id = socket.id;
        socket.broadcast.emit('createShot', data)
    });
});

function player(id, x) {
    this.id = id;
    this.x = x;
}